Imports System.Configuration.ConfigurationSettings
Imports System.Data.OleDb
Imports System.Text
Imports System.IO
Imports System.Web.Mail

Public Class LogFile
    Private Shared errorMailAddress As String = AppSettings("errorMailAddress")
    Private Shared myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")
    Private Shared smtpHost As String = AppSettings("smtpHost")

    Public Shared Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, "yyyy-MM-dd HH:mm:ss") & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, "yyyy-MM-dd") & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine("------------------------------------------------------------")
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

        If errorMailAddress <> "" Then
            SendErrMail(strLine)
        End If
    End Sub

    Public Shared Sub WriteDebug(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strLine As String = "Debug: " & Format(Now, "yyyy-MM-dd HH:mm:ss") & ": " & strMessage
        Dim strFile As String = strLogPath & "\Debug-" & Format(Now, "yyyy-MM-dd") & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file
    End Sub

    Public Shared Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strLine As String = Format(Now, "yyyy-MM-dd HH:mm:ss") & ": " & strMessage
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, "yyyy-MM-dd") & ".log"
        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, "yyyy-MM-dd") & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False)
        Dim w As New StreamWriter(strFileName, append, myEncoding)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Shared Function ReadFile(ByVal strFileName As String) As String
        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, myEncoding)    ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Shared Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

    Public Shared Function MakeSubDirDate(ByRef strFilePath As String, ByVal dtTimeStamp As DateTime) As String
        Dim strSub As String = Format(dtTimeStamp, "yyyy-MM") & "\" & Format(dtTimeStamp, "yyyy-MM-dd")

        Dim strPath As String = Path.GetDirectoryName(strFilePath) & "\" & strSub & "\"
        If Not Directory.Exists(strPath) Then
            Directory.CreateDirectory(strPath)
        End If

        Return strPath & Path.GetFileName(strFilePath)

    End Function

    Private Shared Sub SendErrMail(ByVal strMessage As String)

        Dim message As New MailMessage

        'mail.SmtpServer = "localhost"
        message.Subject = "Error from NTB_SMS_Epost"
        message.Body = strMessage
        message.From = "ntb_sms_epost@ntb.no"
        message.To = errorMailAddress
        message.BodyFormat = MailFormat.Text
        message.BodyEncoding = myEncoding
        Dim mail As SmtpMail
        mail.SmtpServer = smtpHost
        mail.Send(message)

    End Sub

End Class
