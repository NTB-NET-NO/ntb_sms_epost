Imports System.Xml
'Imports System.Xml.XPath
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration.ConfigurationSettings
Imports System.Web.Mail
'Imports System.Text
Imports System.Threading.Thread

Public Class SmsEpostSender

    Protected WithEvents Timer1 As System.Timers.Timer = New System.Timers.Timer
    Protected WithEvents FileSystemWatcher1 As FileSystemWatcher = New FileSystemWatcher

    Const TEXT_INIT As String = "NTB_XmlXsltRoute 'Class XmlRoute' Init finished"

    Protected Const OK = 1
    Protected Const ERR_XML = 2

    Enum DoneStatus
        Delete = 1
        Move = 2
        CopyDelete = 3
        Leave = 4
    End Enum

    Enum InteruptType
        TIMER = 1
        NTFS = 2
        NtfsTimer = 3
    End Enum

    'Public class variabler: 

    Public bBusy As Boolean
    Protected objNews As New News

    'Protected
    Protected intInteruptType As InteruptType = InteruptType.NtfsTimer
    Protected intDoneStatusID As DoneStatus = DoneStatus.Move

    Protected BitnameOfflineFile As String = AppSettings("BitnameOfflineFile")

    Protected bEnableRaisingEvents As Boolean
    Protected bTimerEnabled As Boolean
    Protected bEnableRaisingEventsPause As Boolean
    Protected bTimerEnabledPause As Boolean

    'Internal class variabler: 
    Protected dsCustomer As DataSet
    Protected htCustomer As New Hashtable

    Protected strInputPath As String = AppSettings("strInputPath")
    Protected strInputFilter As String = AppSettings("strInputFilter") ' "*.xml"
    Protected strDonePath As String = AppSettings("strDonePath")
    Protected strOutPath As String = AppSettings("strOutPath")

    Protected databaseOfflinePath As String = AppSettings("databaseOfflinePath")

    Protected intSleepSec As Integer = AppSettings("intSleepSec")
    Protected intPollingInterval As Integer = AppSettings("intPollingInterval")

    Public Sub New()
        Init()
    End Sub

    Protected Sub Init()
        FillDataSets()
        CreateOutDirectories()
        FillCustomerHt()

        Timer1.Interval = intPollingInterval * 1000
        FileSystemWatcher1.Path = strInputPath
        FileSystemWatcher1.Filter = strInputFilter

        Select Case intInteruptType
            Case InteruptType.TIMER
                bTimerEnabled = True
            Case InteruptType.NTFS
                bEnableRaisingEvents = True
            Case InteruptType.NtfsTimer
                bTimerEnabled = True
                bEnableRaisingEvents = True
        End Select
        WriteLog(logFilePath, TEXT_INIT)
    End Sub

    'Public Sub SyncTimer(ByVal syncObj As Object)
    '    Timer1.SynchronizingObject = syncObj
    '    FileSystemWatcher1.SynchronizingObject = syncObj
    'End Sub

    Protected Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        bBusy = True
        Timer1.Enabled = False
        FileSystemWatcher1.EnableRaisingEvents = False
#If DEBUG Then
        Beep()
#End If
        Timer1.Interval = intPollingInterval * 1000
        DoAllInputFiles()
        Timer1.Enabled = bTimerEnabledPause
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEventsPause
        bBusy = False
    End Sub

    Protected Sub FileSystemWatcher1_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher1.Created
        bBusy = True
        FileSystemWatcher1.EnableRaisingEvents = False
        Timer1.Enabled = False
        'Dim strFile As String

#If DEBUG Then
        Beep()
        Console.WriteLine(Now & ": Created: " & e.Name & ", ChangeType: " & e.ChangeType & ", NotifyFilter: " & FileSystemWatcher1.NotifyFilter)
#End If

        'Wait some seconds for last inputfile file to be written by external program, before reading
        'System.Threading.Thread.
        Sleep(intSleepSec * 1000)
        DoOneInputFile(e.FullPath)

        'Do new files which might have appeared during DoOneInputFile
        DoAllInputFiles()
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEventsPause
        Timer1.Enabled = bTimerEnabledPause
        bBusy = False
    End Sub

    Public Sub Start()
        'Do any files waiting in Input Path when starting program
        DoAllInputFiles()
        bTimerEnabledPause = bTimerEnabled
        Timer1.Enabled = bTimerEnabled
        bEnableRaisingEventsPause = bEnableRaisingEvents
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEvents
    End Sub

    Public Sub Pause()
        bTimerEnabledPause = False
        Timer1.Enabled = False
        bEnableRaisingEventsPause = False
        FileSystemWatcher1.EnableRaisingEvents = False
    End Sub

    Public Sub DoAllInputFiles() 'ByRef rowGroupList As DataRow, ByRef intCount As Integer)
        Dim arrFileList() As String
        Dim strInputFile As String
        Static intCount As Long

        'Read all files in folder
#If Not Debug Then
        Try
#End If
        arrFileList = Directory.GetFiles(strInputPath, strInputFilter)
#If Not Debug Then
        Catch e As Exception
            WriteErr(errFilePath, "Feil input directory: " & strInputPath, e)
            Exit Sub
        End Try
#End If
        'Wait some seconds for last inputfile file to be written by external program, before reading
        Sleep(intSleepSec * 1000)
        For Each strInputFile In arrFileList
            DoOneInputFile(strInputFile)
            intCount += 1
            Application.DoEvents()
        Next
    End Sub

    Protected Sub DoOneInputFile(ByVal strInputFile As String)
#If Not Debug Then
        Try
#Else
        Console.WriteLine(Now & ": DoOneInputFile: " & strInputFile)
#End If
        WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
        WriteLog(logFilePath, "Start: " & strInputFile)
        WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
        RouteFile(strInputFile, intDoneStatusID)
#If Not Debug Then
        Catch e As Exception
            WriteErr(errFilePath, "Feilet: " & strInputFile, e)
            Dim strOutPutFile As String = strErrorPath & "\" & Path.GetFileName(strInputFile)
            File.Copy(strInputFile, strOutPutFile)
            File.Delete(strInputFile)
        End Try
#End If
    End Sub

    Protected Function RouteFile(ByVal strFilename As String, ByVal intDoneStatusID As DoneStatus) As Integer
        Dim i As Integer
        Dim strDoneFile As String


        Try
            objNews.LoadXml(strFilename)
            'objSniff.SetXmlText(xmlDoc)
        Catch e As Exception
            Return ERR_XML
        End Try

        For Each objCustomer As Customer In htCustomer
            If CheckForCopy(objCustomer) Then
                CopyTrans(strFilename, objCustomer)
            Else
                'WriteLogNoDate(logFilePath, "       Ikke til: " & rowCustomer("CustomerName"))
            End If
        Next

        Dim strStatus As String
        strDoneFile = strDonePath & Path.DirectorySeparatorChar & Path.GetFileName(strFilename)
        Select Case intDoneStatusID
            Case DoneStatus.CopyDelete
                File.Copy(strFilename, strDoneFile, True)
                File.Delete(strFilename)
                strStatus = "CopyDeleted to: " & strDoneFile
            Case DoneStatus.Move
                File.Move(strFilename, strDoneFile)
                strStatus = "Moved to: " & strDoneFile
            Case DoneStatus.Delete
                File.Delete(strFilename)
                strStatus = "Deleted: " & strFilename
            Case DoneStatus.Leave
                strStatus = "Not Deleted: " & strFilename
                'do nothing
        End Select

        WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
        WriteLog(logFilePath, "Routing finished: " & strStatus)

        Return OK

    End Function

    Protected Sub CreateOutDirectories()
        Directory.CreateDirectory(strOutPath)
        Directory.CreateDirectory(strDonePath)
        Directory.CreateDirectory(strErrorPath)
        Directory.CreateDirectory(strInputPath)
        Directory.CreateDirectory(logFilePath)
    End Sub

    Protected Sub FillDataSets()
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter
        Dim command As OleDbCommand = New OleDbCommand

        dataAdapter.SelectCommand = command
        command.Connection = cn

        dsCustomer = New DataSet

        FillOneDataSet(dsCustomer, "dsCustomer", "SELECT * FROM Customer", dataAdapter, command)

        dataAdapter = Nothing
        command = Nothing

    End Sub

    Protected Sub FillCustomerHt()
        For Each row As DataRow In dsCustomer.Tables(0).Rows
            Dim objCustomer As New Customer(row)
            htCustomer.Add(row("ID"), objCustomer)
        Next
    End Sub

    Protected Function CheckForCopy(ByVal objCustomer As Customer) As Boolean
        'Dim strXpath As String = rowCustomer("XPath") & ""
        Dim customerId As String

        'If strXpath = "" Then
        'Tom xpath gir kopiering uansett
        'Return True
        'End If

        'If CheckXpath(xmlDoc, rowCustomer) Then
        'Return True
        'End If

        ' At last if not found in other methods try Sniff search:
        'customerId = rowCustomer("CustomerID")
        'If objSniff.Find(customerId) Then
        'Return True
        'End If

        Return False

    End Function

    Protected Function CheckXpath(ByRef xmlDoc As XmlDocument, ByRef rowCustomer As DataRow) As Boolean
        Dim nodeList As XmlNodeList
        Dim strCompare As String
        Dim intCompareHit As Integer
        Dim intHits As Integer
        Dim bFound As Boolean

        strCompare = rowCustomer("compare")
        intCompareHit = rowCustomer("hits")
        Dim strXpath As String = rowCustomer("XPath") & ""

        nodeList = xmlDoc.SelectNodes(strXpath)
        intHits = nodeList.Count

        bFound = False
        If intHits = intCompareHit Then
            bFound = True
        End If

        Return bFound
    End Function

    Protected Function CopyTrans(ByVal strFilename As String, ByVal objCustomer As Customer) As Boolean
        Dim strSuffix As String
        Dim strOutFile As String
        Dim tmpFile As String
        Dim row As DataRow
        Dim strErr As String
        Dim bEMail As Boolean

        'Kopier fil til Kundens mappe
        Try
            If bEMail Then
                ' Sender fila som vedlegg i E-post
                Dim message As MailMessage
                If message Is Nothing Then
                    If LCase(strSuffix) = ".htm" Then
                        MakeNewMessage(message, tmpFile, objNews.htmlNews)
                    Else
                        'MakeNewMessage(message, tmpFile, objNews.htmlNews)
                    End If
                End If
                message.To = strOutPath
                SmtpMail.Send(message)
            Else
                'Copy file to destination
                strOutFile = strOutPath & Path.DirectorySeparatorChar & Path.GetFileNameWithoutExtension(strFilename) & strSuffix
                File.Copy(tmpFile, strOutFile, True)
            End If
        Catch e As Exception
            strErr = e.Message & e.StackTrace
            WriteErr(logFilePath, "Feil i filkopiering", e)
            'MsgBox(strErr)
            Return False
        End Try
        WriteLog(logFilePath, "Kopiert til: " & objCustomer.Customer & ": " & Path.GetDirectoryName(strOutFile))

        Return True
    End Function

    Sub MakeNewMessage(ByRef message As MailMessage, ByVal tmpFile As String, ByVal xmlDoc As XmlDocument, Optional ByVal strHtmlDoc As String = "")
        message = New MailMessage
        'mail.SmtpServer = "localhost"
        message.From = "rov@ntb.no"
        message.BodyEncoding = myEncoding

        Try
            message.Subject = "NTB: " _
            & xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").InnerText & ": " _
            & xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText
        Catch
            message.Subject = "Nyhet fra NTB"
        End Try

        If strHtmlDoc = "" Then
            message.BodyFormat = MailFormat.Text
            message.Body = "Nyhet fra NTB, se vedlegg" & vbCrLf
            Dim attachment As MailAttachment = New MailAttachment(tmpFile)
            message.Attachments.Add(attachment)
        Else
            message.BodyFormat = MailFormat.Html
            message.Body = strHtmlDoc
        End If

    End Sub

    Public Function WaitWhileBusy(ByVal SleepRetrySeconds As Integer, Optional ByVal TimeOutSeconds As Integer = 15) As Boolean
        Dim i As Integer
        For i = 0 To TimeOutSeconds / SleepRetrySeconds
            If bBusy Then
                Sleep(SleepRetrySeconds * 1000)
            Else
                Return True
            End If
        Next
        Return False
    End Function

End Class
