' Siste endret 2005.07.03 av Roar Vestre
Imports System.Xml
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Public Class WapIndex
    Private Shared myEncoding As System.Text.Encoding = System.Text.Encoding.GetEncoding("iso-8859-1")
    Private wapPath As String = AppSettings("wapPath")
    Private wapInitPath As String = AppSettings("wapInitPath")

    Private logFilePath As String = AppSettings("logFilePath")
    Private wapIndexXml As New XmlDocument
    Private wmlFile As String
    Private wmlInitFile As String

    Private Const WAP_HEADER As String = "<?xml version='1.0' encoding='iso-8859-1'?>" _
     & "<!DOCTYPE wml PUBLIC '-//WAPFORUM//DTD WML 1.1//EN' 'http://www.wapforum.org/DTD/wml_1.1.xml'>" & vbCrLf

    Private sokeSideURL As String = AppSettings("sokeSideURL")

    Private WAP_TOP As String = "<wml>" _
        & "<template><do type='prev' name='Previous' label='Previous'><prev/></do></template>" _
        & "<card id='card1' title='NTB' newcontext='true'>" _
        & "<p align='right'><i><a href='" & sokeSideURL & "'>&lt;- til s�keside</a></i></p>" & vbCrLf

    Private Const WAP_BOTTOM As String = vbCrLf & "</card></wml>"
    Private EMPTY_WAP As String = WAP_TOP & "<p></p>" & WAP_BOTTOM
    'Private Const EMPTY_WAP As String = "<wml>" _
    '    & "<template><do type='prev' name='Previous' label='Previous'><prev/></do></template>" _
    '    & "<card id='card1' title='NTB' newcontext='true'>" _
    '    & "<p align='right'><i><a href='../hent'>&lt;- til s�keside</a></i></p>" & vbCrLf _
    '    & "<p></p>" & vbCrLf _
    '    & "</card></wml>"

    Public Sub New(ByVal UserPhone As String)
        'wapIndexXml.PreserveWhitespace = False
        If UserPhone = "" Then
            wmlFile = wapPath & "\NTB.wml"
            wmlInitFile = wapInitPath & "\NTB.wml"
        Else
            wmlFile = wapPath & "\" & UserPhone & "\NTB.wml"
            wmlInitFile = wapInitPath & "\" & UserPhone & "\NTB.wml"
        End If

        wapIndexXml.PreserveWhitespace = True

        Try
            Dim sr As New StreamReader(wmlInitFile, myEncoding)    ' File.OpenText("log.txt")
            Dim tempWmlHead As String = sr.ReadLine
            wapIndexXml.LoadXml(sr.ReadToEnd)
            sr.Close()
        Catch ex As Exception
            LogFile.MakePath(wapInitPath & "\" & UserPhone)
            LogFile.MakePath(wapPath & "\" & UserPhone)
            wapIndexXml.LoadXml(EMPTY_WAP)
            LogFile.WriteFile(wmlFile, WAP_HEADER & EMPTY_WAP)
        End Try

    End Sub

    Public Sub UpdateWapIndex(ByVal newsWapFile As String, ByVal tittel As String, ByVal timeStamp As Date, ByVal indexFileLines As String)
        Dim tid As String = Format(timeStamp, "HH:mm")
        Dim dato = Format(timeStamp, "dd.MM.yyyy")
        Dim antLinjer As Integer = Val(indexFileLines)
        If antLinjer = 0 Then
            ' Setter default antall linjer til 20
            antLinjer = 20
        End If

        Try
            If tittel.Length > 55 Then
                tittel = tittel.Substring(0, 52) & "..."
            End If
            tittel = tittel.Replace("&", "&amp;")
            tittel = tittel.Replace("'", "&apos;")
            tittel = tittel.Replace("""", "&quot;")
            tittel = tittel.Replace("<", "&lt;")

            Dim pNode As XmlNode = wapIndexXml.SelectSingleNode("/wml/card/p[2]")
            Dim newPNode As XmlNode = wapIndexXml.CreateDocumentFragment

            newPNode.InnerXml = "<a href='" & newsWapFile & "'>" _
                    & tid & " " & tittel & "</a><br/>"

            Dim newDatoNode As XmlNode = wapIndexXml.CreateDocumentFragment
            newDatoNode.InnerXml = "<strong>" & dato & "</strong><br/>"
            If pNode.SelectNodes("*").Count = 0 Then
                'Ingen dato-header er satt:
                pNode.AppendChild(newDatoNode)
            Else
                Dim lastDate As Date = Convert.ToDateTime(pNode.FirstChild.InnerText, New System.Globalization.CultureInfo("nb-NO", True))
                If DateTime.Compare(lastDate, timeStamp.Date) < 0 Then
                    pNode.InsertBefore(newDatoNode, pNode.FirstChild)
                End If
            End If

            ' Setter inn nyhet-link
            pNode.InsertAfter(newPNode, pNode.FirstChild.NextSibling)

            Do
                ' Sletter hvis flere enn antLinjer
                If pNode.SelectNodes("a").Count > antLinjer Then
                    ' Sletter nyhets-link og en extra gang for </br>
                    pNode.RemoveChild(pNode.LastChild)
                    pNode.RemoveChild(pNode.LastChild)
                    If pNode.LastChild.Name = "strong" Then
                        ' Sletter eventuell dato header og en extra gang for </br>
                        pNode.RemoveChild(pNode.LastChild)
                        pNode.RemoveChild(pNode.LastChild)
                    End If
                Else
                    Exit Do
                End If
            Loop

            'Dim strWmlDoc As String = WAP_HEADER & wapIndexXml.OuterXml.Replace("/><a", "/>" & vbCrLf & "<a")

            Dim strWmlDoc As String = WAP_HEADER & WAP_TOP & pNode.OuterXml & WAP_BOTTOM

            strWmlDoc = strWmlDoc.Replace("/><a", "/>" & vbCrLf & "<a")
            strWmlDoc = strWmlDoc.Replace("/><strong>", "/>" & vbCrLf & "<strong>")

            LogFile.WriteFile(wmlInitFile, strWmlDoc)
            LogFile.WriteFile(wmlFile, strWmlDoc)

        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feilet i WapIndex.UpdateWapIndex: " & newsWapFile, ex)
        End Try
    End Sub

    Public Sub DeleteOldLines(ByVal days As Integer)
        Dim dirty As Boolean = False
        Try
            Dim dato As Date = Today.AddDays(days * -1)

            Dim nodeList As XmlNodeList = wapIndexXml.SelectNodes("/wml/card/p[2]/*")
            Dim n As Integer
            For Each node As XmlNode In nodeList
                If node.Name = "strong" Then
                    Dim lastDate As Date = Convert.ToDateTime(node.InnerText, New System.Globalization.CultureInfo("nb-NO", True))
                    If Date.Compare(lastDate, dato) < 0 Then
                        Dim pNode As XmlNode = wapIndexXml.SelectSingleNode("/wml/card/p[2]")
                        Dim last = nodeList.Count - 1
                        For i As Integer = n To last
                            pNode.RemoveChild(pNode.LastChild)
                        Next
                        dirty = True
                        Exit For
                    End If
                End If
                n += 1
            Next
            If dirty Then
                LogFile.WriteFile(wmlInitFile, WAP_HEADER & wapIndexXml.OuterXml)
                LogFile.WriteFile(wmlFile, WAP_HEADER & wapIndexXml.OuterXml)
            End If
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feilet i WapIndex.DeleteOldLines: ", ex)
        End Try
    End Sub

End Class
