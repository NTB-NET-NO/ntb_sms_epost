Imports System.Xml
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports System.IO

Public Class XsltProc
    Private xsltPath As String = AppSettings("xsltPath")
    Private xsltFileHtml As String = AppSettings("xsltFileHtml")
    Private xsltFileWAP As String = AppSettings("xsltFileWAP")
    Private logFilePath As String = AppSettings("logFilePath")

    Public xsltProcHtml As New Xsl.XslTransform
    Public xsltProcWAP As New Xsl.XslTransform
    Private myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")

    Public Sub New()
        Try
            xsltProcHtml.Load(xsltPath & "\" & xsltFileHtml)
            xsltProcWAP.Load(xsltPath & "\" & xsltFileWAP)
        Catch e As Xsl.XsltException
            LogFile.WriteErr(logFilePath, "Feil i XsltTransform", e)
        End Try
    End Sub

    Enum xsltType
        html = 1
        wap = 2
    End Enum

    Public Function DoXslTransform(ByVal xmlDoc As XmlDocument, ByVal type As xsltType) As String

        Try
            Dim swOutput As IO.Stream = New IO.MemoryStream
            Dim resolver As XmlResolver

            Select Case type
                Case xsltType.html
                    xsltProcHtml.Transform(xmlDoc, Nothing, swOutput, resolver)
                Case xsltType.wap
                    xsltProcWAP.Transform(xmlDoc, Nothing, swOutput, resolver)
            End Select

            Dim reader As StreamReader = New StreamReader(swOutput, myEncoding)
            swOutput.Position = 0
            Dim result As String = reader.ReadToEnd
            reader.Close()
            Return result

        Catch e As Xsl.XsltException
            Dim strErr As String = "Error: Line number: " & e.LineNumber & ", Position: " & e.LinePosition & vbCrLf & e.Message & e.StackTrace
            LogFile.WriteErr(logFilePath, "Feil i XsltProc.DoXslTransform: ", e)
            Return strErr
        End Try

    End Function

End Class
