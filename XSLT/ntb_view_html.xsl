<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes"/>
<!-- 
	XSLT Stylesheet by NTB Roar Vestre 
	Last Change 02.04.2002 by RoV
	Last Change 15.04.2002 by RoV, Added Byline, moved "Publisert: ..., 
		and Added handling of empty content p-tags, and Changed styles
	Last Change 31.05.2002 by RoV, Changed Pubdate and added tables
-->

<xsl:template match="/nitf">
<html>
<head>
<!--<link href="http://devserver1/ntb/include/ntb.css" type="text/css" rel="stylesheet" />-->
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
<style>
<xsl:comment>
.viewBody
{
    FONT-WEIGHT: normal;
    COLOR: #000000;
    LINE-HEIGHT: normal;
    FONT-STYLE: normal;
    FONT-FAMILY: Myriad, Arial, Helvetica, Geneva, Swiss, SunSans-Regular;
    TEXT-DECORATION: none
}

.viewHeadline
{
    FONT-WEIGHT: bold;
    FONT-SIZE: small;
    MARGIN-BOTTOM: 10px
}
.viewDate
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 10px
}
.viewIngress
{
    MARGIN-TOP: 7px;
    FONT-WEIGHT: bold;
    FONT-SIZE: x-small;
    MARGIN-BOTTOM: 5px
}
.viewHl2
{
    MARGIN-TOP: 8px;
    FONT-WEIGHT: bold;
    FONT-SIZE: x-small;
    FONT-STYLE: italic;
}
.viewNews
{
    FONT-SIZE: x-small;
    PADDING-TOP: 8px;
}
.viewIndent
{
    FONT-SIZE: x-small;
    TEXT-INDENT: 20px;
}
.viewTable
{
    FONT-SIZE: x-small;
}
.viewTableHead
{
    FONT-SIZE: xx-small;
    FONT-STYLE: italic;
}
.viewEdMsg
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    COLOR: #d33366
}
.viewTagline
{
    MARGIN-TOP: 10px;
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    FONT-STYLE: italic
}
.viewByline
{
    FONT-SIZE: x-small;
    FONT-STYLE: italic
}
.viewUthev
{
    FONT-WEIGHT: bold;
    background-color: #ffff00;
}
.viewStoffGr
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    COLOR: #0000ff
}
</xsl:comment>
</style>
</head>
<body class='viewBody'>
	<!-- Denne kan kommenteres ut hvis man ikke �nsker Stoffgruppe/kategori i toppen av meldingen -->
	<xsl:apply-templates select="head/tobject "/>

	<div class="viewHeadline"><xsl:value-of select="body/body.head/hedline/hl1"/></div>
	<xsl:apply-templates select="body/body.content/media[@media-type='audio']"/>

	<xsl:apply-templates select="head/docdata/ed-msg"/>
	
	<xsl:apply-templates select="body/body.head/byline"/>

	<div class="viewDate">
		Publisert:
		<xsl:value-of select="substring(head/pubdata/@date.publication, 7, 2)"/>.<xsl:value-of select="substring(head/pubdata/@date.publication, 5, 2)"/>.<xsl:value-of select="substring(head/pubdata/@date.publication, 1, 4)"/>
		&#160;<xsl:value-of select="substring(head/pubdata/@date.publication, 10, 2)"/>:<xsl:value-of select="substring(head/pubdata/@date.publication, 12, 2)"/>
		<!--<xsl:value-of select="head/meta[@name='ntb-date']/@content"/>-->
	</div>
	
	<xsl:apply-templates select="body/body.content"/>
	
	<xsl:apply-templates select="body/body.end/tagline"/>

	<xsl:if test="body/body.content/media[@media-type='image' and not(@class)]">
		<hr />
		<xsl:apply-templates select="body/body.content/media[@media-type='image' and not(@class)]"/>
	</xsl:if>

	<xsl:if test="body/body.content/media[@class='prm']">
	<!-- Vedlegg for PRM-meldinger fra NTB+ -->
		<hr />
		<div class="viewIngress">Vedlegg:</div>
		<table>
		<xsl:apply-templates select="body/body.content/media[@class='prm']"/>
		</table>
	</xsl:if>
</body>
</html>
</xsl:template>


<!-- Templates -->

<xsl:template match="head/tobject ">
	<div class='viewStoffGr'>
	<!--<xsl:text>Stoffgr. /undergr. : </xsl:text>-->
	<xsl:value-of select="@tobject.type"/>
	<xsl:text> / </xsl:text>
	<xsl:value-of select="tobject.property/@tobject.property.type"/><br/>
	<xsl:apply-templates select="tobject.subject"/>
	</div>
</xsl:template>

<xsl:template match="tobject.subject[@tobject.subject.type]">
	<!--<xsl:text>Hovedkat./underkat.: </xsl:text>-->
	<xsl:value-of select="@tobject.subject.type"/>
	<xsl:text>; </xsl:text>
</xsl:template> 

<xsl:template match="tobject.subject[@tobject.subject.matter]">
	<!--<xsl:text>Hovedkat./underkat.: </xsl:text>-->
	<br/>
	<xsl:value-of select="@tobject.subject.code"/>
	<xsl:if test="@tobject.subject.matter!=''">
		<xsl:text> / </xsl:text>
		<xsl:value-of select="@tobject.subject.matter"/>
	</xsl:if>
</xsl:template> 

<xsl:template match="head/docdata/ed-msg">
	<xsl:if test="normalize-space(@info)!=''">
	<div class="viewEdMsg">
	Til red:
	<xsl:value-of select="@info"/>
	</div>
	</xsl:if>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:choose>
	<xsl:when test="p">
		<xsl:apply-templates select="p | hl2 | table | br"/>
	</xsl:when>	
	<xsl:otherwise>
		<div class='viewNews'><xsl:value-of select="."/></div>
	</xsl:otherwise>
	</xsl:choose>
		
</xsl:template>

<xsl:template match="body/body.head/byline/*">
	<div class='viewByline'><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="p[.!='']">
	<!-- Normal paragraphs -->
	<div class="viewNews">
	<xsl:apply-templates select="text() | *"/>
	</div>
</xsl:template>

<xsl:template match="p[.='']">
	<!-- Empty paragraphs -->
	<br/>
</xsl:template>

<xsl:template match="p[@lede='true' and . !='']">
	<!-- Paragraph of "ingress" -->
	<div class="viewIngress">
	<xsl:apply-templates select="text() | *"/>
	</div>
</xsl:template>

<xsl:template match="p[@innrykk='true' or @class='txt-ind']">
	<!-- Paragraph of "Br�dtekst innrykk" -->
	<div class="viewIndent">
	<xsl:apply-templates select="text() | *"/>
	</div>
</xsl:template>

<xsl:template match="p[@style='tabellkode' or @class='table-code']">
	<!-- Paragraph of "tabellkode" -->
	<div class="viewTableHead"><xsl:value-of select="."/></div>
</xsl:template>

<!-- EM-phasize and A tags -->
<xsl:template match="em[@class = 'bold']">
	<b><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="em[@class = 'underline']">
	<u><xsl:value-of select="."/></u>
</xsl:template>

<xsl:template match="em[@class = 'italic']">
	<i><xsl:value-of select="."/></i>
</xsl:template>

<xsl:template match="a">
	<a>
	<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
	<xsl:attribute name="target"><xsl:text>_blank</xsl:text></xsl:attribute>
	<xsl:value-of select="."/>
	</a>
</xsl:template>


<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
	<!-- Normal paragraphs -->
	<div class="viewNews"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="hl2">
	<!-- Mellomtittel -->
	<div class="viewHl2"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="table">
	<!-- Tabeller -->
	<table class="viewTable" width="70%" border="1" cellpadding="2" cellspacing="0">
		<xsl:copy-of select="./*"/>
	</table>
</xsl:template>

<xsl:template match="tagline">
	<!-- Article author e-mail (in NTB used as signature) -->
	<div class="viewTagline">
	<xsl:choose>
	<xsl:when test="a/@href">
	<a><xsl:attribute name="href">mailto:<xsl:value-of select="a"/>
		?subject=Ang�ende: <xsl:value-of select="//body/body.head/hedline/hl1"/>
		</xsl:attribute>
		<xsl:value-of select="a"/></a>
	</xsl:when>		
	<xsl:otherwise>
	<xsl:value-of select="a"/>		
	</xsl:otherwise>
	</xsl:choose>
	</div>
</xsl:template> 

<xsl:template match="body/body.content/media[@media-type='image' and not(@class)]">
	<!-- Template for Scanpix media	-->
	<div class="viewIngress">Bilde: <xsl:value-of select="media-reference/@source"/></div>
	<div class="viewNews"><xsl:value-of select="media-caption"/></div>
</xsl:template>

<xsl:template match="body/body.content/media[@class='prm']">
	<!-- Template for PRM media	-->
	<tr><td class="viewNews"><a target="_blank">
	<xsl:attribute name="href">http://193.75.33.34/prm_vedlegg/vedlegg/<xsl:value-of select="media-reference/@alternate-text"/>/<xsl:value-of select="media-reference/@source"/></xsl:attribute>
	<xsl:value-of select="media-reference/@name"/></a>
	</td><td class="viewNews"><xsl:value-of select="media-caption"/></td></tr>
</xsl:template>

<xsl:template match="body/body.content/media[@media-type='audio']">
	<!--
		Hardcoded paths for Image and JavaScript (complete URL is included in NTB's NITF)
	-->
<script language="javascript" src="http://194.19.39.29/kunde/ntb/flashsound.js"></script>
<script language="javascript">var lyd = new FlashSound();</script>
	<a href="javascript://">
	<!--
		<a><xsl:attribute name="href">http://194.19.39.29/kunde/ntb/mp3/<xsl:value-of select="/nitf/head/meta[@name='ntb-lyd']/@content"/>.mp3</xsl:attribute>
	-->
		<xsl:attribute name="onmouseover">lyd.TGotoAndPlay('/','start')</xsl:attribute>
		<xsl:attribute name="onmouseout">lyd.TGotoAndPlay('/','stop')</xsl:attribute>
		<img src="../images/sound_large.gif" border="0" align="right"/>
	</a>
<script>lyd.embedSWF("<xsl:value-of select="media-reference[@mime-type='application/x-shockwave-flash']/@source"/>");</script>

</xsl:template>


</xsl:stylesheet>