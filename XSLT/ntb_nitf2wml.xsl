<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1" 
	omit-xml-declaration="no" standalone="yes" indent="yes" 
	doctype-system='http://www.wapforum.org/DTD/wml_1.1.xml' 
	doctype-public='-//WAPFORUM//DTD WML 1.1//EN' />

<!--<!DOCTYPE wml PUBLIC '-//WAPFORUM//DTD WML 1.1//EN' 'http://www.wapforum.org/DTD/wml_1.1.xml'>-->

<!-- 
	XSLT Stylesheet by NTB Roar Vestre 
	Last Change 02.04.2002 by RoV
	Last Change 15.04.2002 by RoV, Added Byline, moved "Publisert: ..., 
		and Added handling of empty content p-tags, and Changed styles
	Last Change 31.05.2002 by RoV, Changed Pubdate and added tables
-->

<xsl:template match="/nitf">
<wml>
	<template>
		<do type="prev" name="Previous" label="Previous">
			<prev/>
		</do>
	</template>
	<card id="Card01">
		<xsl:attribute name="title">
			<xsl:value-of select="head/docdata/key-list/keyword/@key"/>
		</xsl:attribute>
		<p><strong><xsl:value-of select="body/body.head/hedline/hl1"/></strong></p>
		<xsl:apply-templates select="head/docdata/ed-msg"/>
		<xsl:apply-templates select="body/body.head/byline"/>
		<p align='right'><i><xsl:text>Publ: </xsl:text> 
			<xsl:value-of select="substring(head/pubdata/@date.publication, 7, 2)"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="substring(head/pubdata/@date.publication, 5, 2)"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="substring(head/pubdata/@date.publication, 1, 4)"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="substring(head/pubdata/@date.publication, 10, 2)"/>
			<xsl:text>:</xsl:text>
			<xsl:value-of select="substring(head/pubdata/@date.publication, 12, 2)"/>
			</i>
		</p>
		<xsl:apply-templates select="body/body.content"/>
		<xsl:apply-templates select="body/body.end/tagline"/>
	</card>
</wml>
</xsl:template>

<!-- Templates -->

<xsl:template match="head/docdata/ed-msg">
	<xsl:if test="normalize-space(@info)!=''">
	<p>
	<xsl:text>Til red: </xsl:text>
	<xsl:value-of select="@info"/>
	</p>
	</xsl:if>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:choose>
	<xsl:when test="p">
		<xsl:apply-templates select="p | hl2 | table | br"/>
	</xsl:when>	
	<xsl:otherwise>
		<p><xsl:value-of select="."/></p>
	</xsl:otherwise>
	</xsl:choose>
		
</xsl:template>

<xsl:template match="body/body.head/byline/*">
	<p><xsl:value-of select="."/></p>
</xsl:template>

<xsl:template match="p[.!='']">
	<!-- Normal paragraphs -->
	<p>
	<xsl:apply-templates select="text() | *"/>
	</p>
</xsl:template>

<xsl:template match="p[.='']">
	<!-- Empty paragraphs -->
	<br/>
</xsl:template>

<xsl:template match="p[@lede='true' and . !='']">
	<!-- Paragraph of "ingress" -->
	<p><i><xsl:apply-templates select="text() | *"/></i></p>
</xsl:template>

<xsl:template match="p[@innrykk='true' or @class='txt-ind']">
	<!-- Paragraph of "Br�dtekst innrykk" -->
	<p>
	<xsl:text>&#160;&#160;&#160;&#160;</xsl:text>
	<xsl:apply-templates select="text() | *"/>
	</p>
</xsl:template>

<xsl:template match="p[@style='tabellkode' or @class='table-code']">
	<!-- Paragraph of "tabellkode" -->
	<p><xsl:value-of select="."/></p>
</xsl:template>

<!-- EM-phasize and A tags -->
<xsl:template match="em[@class = 'bold']">
	<strong><xsl:value-of select="."/></strong>
</xsl:template>

<xsl:template match="em[@class = 'underline']">
	<u><xsl:value-of select="."/></u>
</xsl:template>

<xsl:template match="em[@class = 'italic']">
	<i><xsl:value-of select="."/></i>
</xsl:template>

<xsl:template match="hl2">
	<!-- Mellomtittel -->
	<p><strong><xsl:value-of select="."/></strong></p>
</xsl:template>

<!-- Tabeller:
<xsl:template match="table">
	<table class="viewTable" width="70%" border="1" cellpadding="2" cellspacing="0">
		<xsl:copy-of select="./*"/>
	</table>
</xsl:template>
-->
<xsl:template match="tagline">
	<!-- Article author e-mail (in NTB used as signature) -->
	<p>
	<xsl:value-of select="a"/>		
	</p>
</xsl:template> 

</xsl:stylesheet>