Imports System.Text.RegularExpressions

Public Class UthevSokeord

    Dim strStartPattern As String = "(\b"
    ' slutt pattern med norsk stemming av substantiver:
    Dim strEndPattern As String = "(er|ene|e|en|et|a|s|ne|t|r)?\b)"

    Private Const END_CODE As String = "<font class='viewUthev'>"
    Private Const START_CODE As String = "</font>"

    Dim strRegExReplace As String = END_CODE & "$&" & START_CODE

    Public Property regExReplace() As String
        Get
            Return strRegExReplace
        End Get
        Set(ByVal Value As String)
            strRegExReplace = Value
        End Set
    End Property

    Public Property startPattern() As String
        Get
            Return strStartPattern
        End Get
        Set(ByVal Value As String)
            strStartPattern = Value
        End Set
    End Property

    Public Property endPattern() As String
        Get
            Return strEndPattern
        End Get
        Set(ByVal Value As String)
            strEndPattern = Value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Function UthevRegEx(ByVal doc As String, ByVal sokeord As String) As String
        ' For uthevning av s�keord i HTML fil 
        ' Flere s�keord kan bringes inn kommaseparert.

        If sokeord = "" Or sokeord.Length <= 2 Then
            Return doc
        End If

        Dim docRes As String
        Dim sok As String

        sokeord = sokeord.Replace(" ", ",")
        Dim sokeOrdliste() As String = Split(sokeord, ",")

        ' exception list for html code
        Dim exceptionList As String = " div class href mailto subject "

        For Each strSok As String In sokeOrdliste
            strSok = strSok.Trim
            If exceptionList.IndexOf(" " & strSok & " ") = -1 And strSok.Length > 1 Then
                sok &= "|" & strSok
            End If
        Next
        sok = sok.Substring(1)

        sok = "(" & sok & ")"

        docRes = doc

        Dim reg As New Regex(strStartPattern & sok & strEndPattern, RegexOptions.IgnoreCase)

        docRes = reg.Replace(docRes, strRegExReplace)

        Return docRes

    End Function

End Class
