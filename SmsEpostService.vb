Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Data.OleDb
Imports System.ServiceProcess
Imports System.Configuration.ConfigurationSettings
Imports System.Threading.Thread

#If Debug Then
Public Module NTB_SMS_Epost
#Else
Public Class NTB_SMS_Epost : Inherits ServiceBase
#End If

    Const TEXT_STARTED As String = "NTB_SMS_Epost Service Started"
    Const TEXT_STOPPED As String = "NTB_SMS_Epost Service Stopped"
    Const TEXT_INIT As String = "NTB_SMS_Epost Init finished"

    Dim Debug
    '-- Lokale variabler --
    Private objSmsEpost As SmsEpost
    Private logFilePath As String = AppSettings("logFilePath")

#If DEBUG Then
    ' For debug: Run as Console

    Public Sub Main()
        Init()
        Start()
        Console.WriteLine("MENY: ")
        Console.WriteLine("P = Pause")
        Console.WriteLine("Q = Quit: ")
        Console.Write("Trykk menyvalg: ")
        Dim strTest As String
        Do
            strTest = Console.ReadLine
            Select Case UCase(strTest)
                Case "Q"
                    Exit Do
                Case "P"
                    StopWait()
                    Console.WriteLine("MENY: ")
                    Console.WriteLine("S = Start")
                    Console.WriteLine("R = Reinit")
                    Console.WriteLine("Q = Quit: ")
                    Console.Write("Trykk menyvalg: ")
                Case "S"
                    Start()
                    Console.WriteLine("MENY: ")
                    Console.WriteLine("P = Pause")
                    Console.WriteLine("Q = Quit: ")
                    Console.Write("Trykk menyvalg: ")
                Case "R"
                    StopWait()
                    objSmsEpost.ReInitKunde()
                    Start()
                    Console.WriteLine("MENY: ")
                    Console.WriteLine("P = Pause")
                    Console.WriteLine("Q = Quit: ")
                    Console.Write("Trykk menyvalg: ")
            End Select
        Loop
        StopWait()
    End Sub
#Else
    ' For Release: run as service

    Public Shared Sub Main()
        ServiceBase.Run(New NTB_SMS_Epost())
    End Sub

    Public Sub New()
        MyBase.New()
        CanPauseAndContinue = False
        CanStop = True
        ServiceName = "NTB_SMS_Epost"
    End Sub

    Protected Overrides Sub OnStop()
        StopWait()
        EventLog.WriteEntry(TEXT_STOPPED)
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Init()
            Start()
            EventLog.WriteEntry(TEXT_STARTED)
        Catch e As Exception
            LogFile.WriteErr(logFilePath, "Feil i initiering av programmet", e)
            End
        End Try
    End Sub
#End If

    Private Sub Init()
        Dim intCount As Integer
        Dim dtStartTime As Date
        Dim rowGroupList As DataRow

        dtStartTime = Now

#If Not Debug Then
        Try
#End If
        Directory.CreateDirectory(logFilePath)
        objSmsEpost = New SmsEpost
        LogFile.WriteLogNoDate(logFilePath, "===========================================================================================================================")
        LogFile.WriteLog(logFilePath, TEXT_INIT)
#If Not Debug Then
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i initiering av SmsEpost", ex)
            End
        End Try
#End If

    End Sub

    Private Sub StopWait()
        objSmsEpost.StopEvents()
        'objSmsEpost.WaitWhileBusy(2)
        LogFile.WriteLogNoDate(logFilePath, "===========================================================================================================================")
        LogFile.WriteLog(logFilePath, TEXT_STOPPED)
    End Sub

    Private Sub Start()
        LogFile.WriteLog(logFilePath, TEXT_STARTED)
        'objSmsEpost.DoAllInputFiles()
        objSmsEpost.StartTimer1()
        'objSmsEpost.StartEvents()
    End Sub

#If DEBUG Then
End Module
#Else
End Class
#End If
