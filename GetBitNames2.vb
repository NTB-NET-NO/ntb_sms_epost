Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports System.Xml

Public Class GetBitNames2

    ' All Keys are converted to LowerCase and are looked up as LowerCase to avoid confusion of mixed cases

    Private htBitCat As New Hashtable
    Private htBitSubcat As New Hashtable
    Private htBitCatSms As New Hashtable
    Private htBitSubcatSms As New Hashtable
    Private htBitFolders As New Hashtable
    Private connectionString As String = AppSettings("SqlConnectionNTB")
    Private logFilePath As String = AppSettings("logFilePath")
    Private databaseOfflinePath As String = AppSettings("databaseOfflinePath")

    Public Sub New()
        Init()
    End Sub

    Public Sub Init()
        Dim cn As New SqlConnection(connectionString)

        Try
            cn.Open()
            InitBitnames(cn)
        Catch e As Exception
            LogFile.WriteErr(logFilePath, "Feil i GetBitNames2.Init: ", e)
        End Try

        ' Close the connection when done with it.
        cn.Close()
    End Sub

    Private Sub InitBitnames(ByRef cn As SqlConnection)
        Dim dataAdapter As New SqlDataAdapter
        Dim mySqlCommand As New SqlCommand

        dataAdapter.SelectCommand = mySqlCommand
        mySqlCommand.Connection = cn

        Dim dsBitnames As New DataSet

        FillOneDataSet(dsBitnames, "dsBitnames", "getBitnamesSmsEpost", dataAdapter, mySqlCommand)

        Dim strKey As String
        Dim strKeySms As String
        Dim intCategory As Integer
        Dim lngSubcat As Long ' Int64 
        Dim strXPath As String

        '*** Fill Bitmaps from table BITNAMES: for Category and Subcategory into Hastables
        For Each row As DataRow In dsBitnames.Tables(0).Rows
            intCategory = CInt(row("Category") & "")
            lngSubcat = CLng(row("Subcategory") & "")
            strKey = row("NITF_content") & ""
            strKeySms = row("SmsEpostName") & ""

            strKey = strKey.ToLower
            strKeySms = strKeySms.ToLower

            If lngSubcat = 0 Then
                Try
                    htBitCat.Add(strKey, intCategory)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
                Try
                    htBitCatSms.Add(strKeySms, intCategory)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
            Else
                Try
                    htBitSubcat.Add(strKey, lngSubcat)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
                Try
                    htBitSubcatSms.Add(strKeySms, lngSubcat)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
            End If
        Next 'row

        Dim dsNtbFolder As New DataSet

        FillOneDataSet(dsNtbFolder, "dsNtbFolder", "getNtbFolder", dataAdapter, mySqlCommand)

        '*** Fill Bitmaps from table ntbFolder into Hastables
        For Each row As DataRow In dsNtbFolder.Tables(0).Rows
            intCategory = CInt(row("ntbFolderID") & "")
            strKey = row("folderName") & ""

            Try
                htBitFolders.Add(strKey.ToLower, intCategory)
            Catch ex As Exception
                LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
            End Try

        Next

    End Sub

    Private Sub InitBitnames_old(ByRef cn As SqlConnection)
        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand

        mySqlCommand = New SqlCommand("getBitnamesSmsEpost", cn)
        myDataReader = mySqlCommand.ExecuteReader()

        Dim strKey As String
        Dim strKeySms As String
        Dim intCategory As Integer
        Dim intSubcat As Long ' Int64 
        Dim test As Boolean = (intSubcat And intSubcat)
        Dim strXPath As String

        '*** Fill Bitmaps from table BITNAMES: for Category and Subcategory into Hastables
        Do While (myDataReader.Read())

            intSubcat = myDataReader.GetInt64(3)
            strKey = LCase(myDataReader.GetString(4) & "")
            strKeySms = LCase(myDataReader.GetString(5) & "")
            intCategory = myDataReader.GetInt32(2)

            If intSubcat = 0 Then
                Try
                    htBitCat.Add(strKey, intCategory)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
                Try
                    htBitCatSms.Add(strKeySms, intCategory)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
            Else
                Try
                    htBitSubcat.Add(strKey, intSubcat)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
                Try
                    htBitSubcatSms.Add(strKeySms, intSubcat)
                Catch ex As Exception
                    LogFile.WriteErr(logFilePath, "Feil i GetBitnames2.InitBitnames: ", ex)
                End Try
            End If
        Loop
        ' Always call Close when done reading.
        myDataReader.Close()

        mySqlCommand = New SqlCommand("getNtbFolder", cn)
        myDataReader = mySqlCommand.ExecuteReader()

        '*** Fill Bitmaps from table ntbFolder into Hastables
        Do While (myDataReader.Read())
            intCategory = myDataReader.GetInt32(0)
            strKey = LCase(myDataReader.GetString(1) & "")
            htBitFolders.Add(strKey, intCategory)
        Loop

        myDataReader.Close()

    End Sub

    Public Function GetBitFolders(ByRef xmlCatNodeList As XmlNodeList) As Integer
        Dim intBitmap As Integer
        For Each xmlCatNode As XmlNode In xmlCatNodeList
            intBitmap = intBitmap Or htBitFolders.Item(xmlCatNode.InnerText.ToLower)
        Next
        Return intBitmap
    End Function

    Public Function GetBitCatNitf(ByRef xmlCatNodeList As XmlNodeList) As Integer
        Dim intBitmap As Integer
        For Each xmlCatNode As XmlNode In xmlCatNodeList
            intBitmap = intBitmap Or htBitCat.Item(xmlCatNode.InnerText.ToLower)
        Next
        Return intBitmap
    End Function

    Public Function GetBitSubCatNitf(ByRef xmlCatNodeList As XmlNodeList) As Long
        Dim intBitmap As Long
        For Each xmlCatNode As XmlNode In xmlCatNodeList
            intBitmap = intBitmap Or htBitSubcat.Item(xmlCatNode.InnerText.ToLower)
        Next
        Return intBitmap
    End Function

    Public Function GetBitMapSmsKunde(ByRef strValgt As String) As Integer
        Dim intBitmap As Integer
        Dim strArray() As String = strValgt.Replace("Kongestoff", "Kuriosa").Split(";")

        For Each strTemp As String In strArray
            strTemp = strTemp.Trim
            If strTemp <> "" Then
                intBitmap = intBitmap Or htBitCatSms.Item(strTemp.ToLower)
            End If
        Next

        Return intBitmap
    End Function

    Public Function GetBitSubCatSmsKunde(ByRef strValgt As String) As Long
        Dim intBitmap As Long
        Dim strArray() As String = strValgt.Split(";")

        For Each strTemp As String In strArray
            strTemp = strTemp.Trim
            If strTemp <> "" Then
                intBitmap = intBitmap Or htBitSubcatSms.Item(strTemp.ToLower)
            End If
        Next

        Return intBitmap
    End Function

    Public Sub FillOneDataSet(ByRef dsDataSet As DataSet, ByVal strOfflineFile As String, ByVal strCommand As String, ByVal dataAdapter As SqlDataAdapter, ByVal command As SqlCommand, Optional ByVal strGroupListID As String = "")
        Dim strFile As String = databaseOfflinePath & "\" & strOfflineFile & strGroupListID & ".xml"
        command.CommandText = strCommand
        Try
            dataAdapter.Fill(dsDataSet)
            dsDataSet.WriteXml(strFile)
        Catch ex As Exception
            dsDataSet.ReadXml(strFile)
            LogFile.WriteErr(logFilePath, "Feilet i GetBitNames2.FillOneDataSet: ", ex)
        End Try
    End Sub
End Class

