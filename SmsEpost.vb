'Sist endret 2005.07.01 av Roar Vestre
'
Imports System.Xml
'Imports System.Xml.XPath
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports System.Text
Imports System.Threading.Thread ' for Sleep funksjonen

Public Class SmsEpost
    Private ntbTjenester As Integer = AppSettings("ntbTjenester")
    Private ignoreMeldingstype As Integer = AppSettings("ignoreMeldingstype")

    Private strErrorPath As String = AppSettings("strErrorPath")
    Private logFilePath As String = AppSettings("logFilePath")
    Private strMailOfflinePath As String = AppSettings("strMailOfflinePath")
    Private smtpHost As String = AppSettings("smtpHost")
    Private sendSms As Boolean = AppSettings("sendSms").ToLower = "true"
    Private sendEpost As Boolean = AppSettings("sendEpost").ToLower = "true"

    Private sqlConnectionString As String = AppSettings("SqlConnectionWap_Kunder")
    Private cn As New SqlConnection(sqlConnectionString)
    Private customerQuery As String = AppSettings("customerQuery")
    Private pollDataBaseIntervalMinutes As Integer = AppSettings("pollDataBaseIntervalMinutes") * 60000

    Private strInputPath As String = AppSettings("strInputPath")
    Private strInputFilter As String = AppSettings("strInputFilter") ' "*.xml"
    Private strDonePath As String = AppSettings("strDonePath")
    Private strSmsPath As String = AppSettings("strSmsPath")
    Private xhtmlWapPath As String = AppSettings("xhtmlWapPath")

    Private databaseOfflinePath As String = AppSettings("databaseOfflinePath")

    Private intSleepSec As Integer = AppSettings("intSleepSec") * 1000
    Private intRetrySec As Integer = AppSettings("intRetrySec") * 1000
    Private intPollingInterval As Integer = AppSettings("intPollingInterval") * 1000

    Private defaultWapIndex As New WapIndex("alle")
    Private wapPath As String = AppSettings("wapPath")
    Private wapInitPath As String = AppSettings("wapInitPath")

    Private wmlNewsPath As String = AppSettings("wmlNewsPath")
    Private wapWmlPath As String = wapPath & "\" & wmlNewsPath
    Private beholdWapDager As String = AppSettings("beholdWapDager")
    Private indexFileLines As String = AppSettings("indexFileLines")

    Private WithEvents FileSystemWatcher1 As FileSystemWatcher = New FileSystemWatcher
    Private WithEvents Timer1 As System.Timers.Timer = New System.Timers.Timer
    Private WithEvents TimerUpdateFromDb As System.Timers.Timer = New System.Timers.Timer
    Private myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")

    'Public class variabler: 
    Public isBusy As Boolean
    Public isStarted As Boolean
    Public isMailOffline As Boolean = True

    Private intDoneStatusID As DoneStatus = DoneStatus.CopyDelete
    'Internal class variabler: 
    Private objNews As News
    Private objBitNames As GetBitNames2
    Private dsKunde As DataSet
    Private htKunde As New Hashtable

    Private Shared mut As New System.Threading.Mutex
    Private objXsltProc As XsltProc

    Enum DoneStatus
        Delete = 1
        Move = 2
        CopyDelete = 3
        Leave = 4
    End Enum

    Public Sub New()
        Init()
    End Sub

    Private Sub Init()
        CreateOutDirectories()
        objBitNames = New GetBitNames2
        objXsltProc = New XsltProc
        FillDataSets()
        FillCustomerHt()

        TimerUpdateFromDb.Interval = pollDataBaseIntervalMinutes

        FileSystemWatcher1.Path = strInputPath
        FileSystemWatcher1.Filter = strInputFilter
        FileSystemWatcher1.NotifyFilter = NotifyFilters.FileName 'Or NotifyFilters.LastWrite
        FileSystemWatcher1.InternalBufferSize = 32768

    End Sub

    Private Sub CreateOutDirectories()
        ' Denne rutinen lager outmapper som filer legges i n�r de har kommet ut. 
        Directory.CreateDirectory(strSmsPath)
        Directory.CreateDirectory(strDonePath)
        Directory.CreateDirectory(strErrorPath)
        Directory.CreateDirectory(strInputPath)
        Directory.CreateDirectory(logFilePath)
        Directory.CreateDirectory(databaseOfflinePath)
        Directory.CreateDirectory(strMailOfflinePath)
        Directory.CreateDirectory(wapPath)
        Directory.CreateDirectory(wapInitPath)
        Directory.CreateDirectory(wapWmlPath)
        Directory.CreateDirectory(xhtmlWapPath)
    End Sub

    Public Sub StartTimer1()
        ' Timer som kun kj�res ved oppstart sor � t�mme eventuell k� av NITF-filer
        Timer1.Interval = 3000
        Me.Timer1.Start()
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        Try
            Timer1.Stop()
            Me.StopEvents()
            DoAllInputFiles()
            Me.StartEvents()
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i SmsEpost.Timer1_Elapsed: " & strInputPath, ex)
        End Try
    End Sub

    Public Sub DoAllInputFiles() 'ByRef rowGroupList As DataRow, ByRef intCount As Integer)
        Dim arrFileList() As String
        Dim strInputFile As String

        'Read all files in folder
        isBusy = True
        FileSystemWatcher1.EnableRaisingEvents = False
        Try
            arrFileList = Directory.GetFiles(strInputPath, strInputFilter)
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i  SmsEpost.DoAllInputFiles: input directory: " & strInputPath, ex)
            Exit Sub
        End Try

        FileSystemWatcher1.EnableRaisingEvents = True
        'Wait some seconds for last inputfile file to be written by external program, before reading
        Sleep(intSleepSec)
        For Each strInputFile In arrFileList
            DoOneInputFile(strInputFile)
        Next
        isBusy = False
    End Sub

    Private Sub FileSystemWatcher1_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher1.Created
        Try
            mut.WaitOne()
            isBusy = True
            'Wait some seconds for last inputfile file to be written by external program, before reading
            Sleep(intSleepSec)
            DoOneInputFile(e.FullPath)
            isBusy = False
            mut.ReleaseMutex()
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feilet i SmsWpost.FileSystemWatcher1_Created: ", ex)
        End Try
    End Sub

    Private Sub DoOneInputFile(ByVal strInputFile As String)
        ' Unng� � �pne fil som er under skrivning eller lesing.
        ' Sleep(50)
        For i As Integer = 1 To 10
            'Pr�ver 10 ganger
            Try
                Dim f As FileStream = File.Open(strInputFile, FileMode.Open, FileAccess.Read, FileShare.None)
                f.Close()
                Exit For
            Catch ex As Exception
                LogFile.WriteDebug(logFilePath, " File already open, Sleep ..........: " & strInputFile)
                Sleep(intRetrySec)
            End Try
        Next

        LogFile.WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
        LogFile.WriteLog(logFilePath, "Leser fila: " & strInputFile)


#If Not Debug Then
        Try
#End If
        RouteFileToCustomers(strInputFile, intDoneStatusID)

#If Not Debug Then
        Catch e As Exception
            LogFile.WriteErr(logFilePath, "Feilet i SmsEpost.DoOneInputFile: " & strInputFile, e)
            Dim strOutPutFile As String = strErrorPath & "\" & Path.GetFileName(strInputFile)
            Try
                File.Copy(strInputFile, strOutPutFile)
                File.Delete(strInputFile)
            Catch ex As Exception
                LogFile.WriteErr(logFilePath, "Fil-errHandler feilet i SmsEpost.DoOneInputFile: " & strInputFile & " to " & strOutPutFile, e)
            End Try
        End Try
#End If
    End Sub

    Private Sub RouteFileToCustomers(ByVal strFilename As String, ByVal intDoneStatusID As DoneStatus)
        ' -----------------------------------------------------------------------------
        ' RouteFileToCustomers(ByVal strFilename As String, ByVal intDoneStatusID As DoneStatus)
        '
        ' Description: This is the main procedure that controls the complete job
        '
        ' Parameters:   strFilename
        '               intDoneStatusID
        '
        ' Returns:
        '
        ' Notes :
        '           05.30.2008: Added documentation-template to routine. 
        ' -----------------------------------------------------------------------------


        Dim i As Integer
        Dim strStatus As String
        Dim strDoneFile As String
        Dim isCustomer As Boolean

        Dim testNtbTjeneste As Integer = ntbTjenester
        Dim testNtbMeldingstype As Integer = ignoreMeldingstype

        objNews = New News(strFilename, objBitNames, objXsltProc, testNtbTjeneste, testNtbMeldingstype)

        If testNtbTjeneste > 0 Then
            'Skriver f�rst til default WAP-indexfil
            objNews.WriteXhtmlWap(xhtmlWapPath)
            defaultWapIndex.UpdateWapIndex(objNews.indexFileRef, objNews.ntbInfolinje, objNews.dtTimeStamp, indexFileLines)

            If testNtbMeldingstype >= 0 Then
                For Each objKunde As Customer In htKunde.Values
                    Dim isSniffFound As Boolean = False
                    If CheckBitmapSniffHit(objKunde, isSniffFound) Then
                        ' Hvis Bitm�nster eller snifftekst stemmer med kunens og Nyheter 
                        ' s� utf�res hovedjoben for Wap SMS og Epost pr. kunde:
                        Me.DoEpostSms(strFilename, objKunde, isSniffFound)
                        isCustomer = True
                    End If
                Next
                If Not isCustomer Then
                    LogFile.WriteLogNoDate(logFilePath, "       Ingen kunder for denne nyheten")
                End If
            Else
                LogFile.WriteLogNoDate(logFilePath, "       Kun WAP-sider (Meldingstype): " & objNews.NTBUndergrupper)
            End If
        Else
            LogFile.WriteLogNoDate(logFilePath, "       Ikke behandlet NTBTjeneste: " & objNews.NTBTjeneste)
        End If

        strDoneFile = strDonePath & Path.DirectorySeparatorChar & Path.GetFileName(strFilename)
        strDoneFile = LogFile.MakeSubDirDate(strDoneFile, objNews.dtTimeStamp)

        Select Case intDoneStatusID
            Case DoneStatus.CopyDelete
                File.Copy(strFilename, strDoneFile, True)
                File.Delete(strFilename)
                strStatus = "CopyDeleted to: " & strDoneFile
            Case DoneStatus.Move
                File.Move(strFilename, strDoneFile)
                strStatus = "Moved to: " & strDoneFile
            Case DoneStatus.Delete
                File.Delete(strFilename)
                strStatus = "Deleted: " & strFilename
            Case DoneStatus.Leave
                strStatus = "Not Deleted: " & strFilename
                'do nothing
        End Select

        'LogFile.WriteLogNoDate(logFilePath, "       " & strStatus)
    End Sub

    Private Function TestWapCustomerUpdate() As Boolean
        Try
            cn.Open()
            ' Testing if any rows in Customer table have been updated:
            Dim mySqlCommand As SqlCommand = New SqlCommand("testSmsEpostCustomerFile", cn)
            Dim isUpdated As Boolean = mySqlCommand.ExecuteScalar()

            If Not isUpdated Then
                cn.Close()
                Return False
            Else
                cn.Close()
                Return True
            End If
        Catch e As Exception
            cn.Close()
            LogFile.WriteErr(logFilePath, "Feil i TestWapCustomerUpdate: ", e)
            Return False
        End Try

    End Function

    Private Function FillDataSets()
        Dim dataAdapter As New SqlDataAdapter
        Dim command As New SqlCommand

        dataAdapter.SelectCommand = command
        command.Connection = cn

        Try
            cn.Open()
        Catch e As Exception
            LogFile.WriteErr(logFilePath, "Feil i OpenDatabase: cn.Open", e)
        End Try

        dsKunde = New DataSet

        FillOneDataSet(dsKunde, "dsCustomer", customerQuery, dataAdapter, command)

        dataAdapter = Nothing
        command = Nothing

        cn.Close()
    End Function

    Private Sub FillCustomerHt()
        htKunde.Clear()
        For Each row As DataRow In dsKunde.Tables(0).Rows
            Dim objKunde As New Customer(row, objBitNames)
            htKunde.Add(row("ID"), objKunde)
        Next
    End Sub

    Private Function CheckBitmapSniffHit(ByVal objKunde As Customer, ByRef isSniffFound As Boolean) As Boolean
        'Dim strXpath As String = rowCustomer("XPath") & ""
        Dim customerId As String
        Dim isTreffStoffgruppe As Boolean

        If (objKunde.Prio13Stoffgr And objNews.Prio13Stoffgr) Then
            ' Send alle hastemeldinger:
            Return True
        End If

        If (objKunde.NTBStoffgrupper And (objNews.NTBStoffgrupper And 4)) Then
            'Hvis nyheten er sport og brukeren har valt stoffgruppe sport
            If objKunde.alleSpo Then Return True

            isTreffStoffgruppe = True
            If objKunde.NTBUnderKatSport = 0 Then
                'Ingen underkategorier valgt

            ElseIf (objKunde.NTBUnderKatSport And objNews.NTBUnderKatSport) Then
                'Treff p� underkategorier sport
                Return True
            End If

        ElseIf (objKunde.NTBStoffgrupper And objNews.NTBStoffgrupper) Then

            isTreffStoffgruppe = True
            'Hvis treff p� andre stoffgrupper:

            If objKunde.alleHov And objKunde.alleOmr Then
                Return True

            ElseIf objKunde.NTBHovedKategorier = 0 And objKunde.NTBOmraader = 0 Then
                'Ingen underkategorier eller omr�der er valgt = Ingen utsendelse
                If objKunde.alleHov Or objKunde.alleOmr Then Return True

            ElseIf objKunde.NTBHovedKategorier = 0 And (objKunde.NTBOmraader And objNews.NTBOmraader) Then
                'Bare omr�der er valgt:
                If TestFylker(objKunde) Then Return True

            ElseIf objKunde.NTBOmraader = 0 And (objKunde.NTBHovedKategorier And objNews.NTBHovedKategorier) Then
                'Hvis bare hovekategorier er valgt:
                If TestUnderKategori(objKunde) Then Return True

            ElseIf (objKunde.NTBHovedKategorier And objNews.NTBHovedKategorier) And (objKunde.NTBOmraader And objNews.NTBOmraader) Then
                'B�de hovedkategorier og omr�der er valgt:
                If TestFylker(objKunde) And TestUnderKategori(objKunde) Then Return True

            End If
        End If

        ' At last if not found in other methods try Sniff search:
        ' Hvis kunde har s�keord og 
        ' nyhetstreff innenfor valgt stoffgruppe eller ingen stoffgruppe er valgt
        If objKunde.doSniff And (isTreffStoffgruppe Or objKunde.NTBStoffgrupper = 0) Then
            If objKunde.FinnSniff(objNews.txtNews) Then
                isSniffFound = True
                Return True
            End If
        End If

        Return False

    End Function

    Private Function TestUnderKategori(ByVal objKunde As Customer) As Boolean
        Const HEX_SPO = &H800
        Const HEX_KUL = &H10
        Const HEX_KUR = &H20
        Const HEX_OKO = &H10000
        Const HEX_SUBKAT As Long = &H10830

        If objKunde.alleHov Then
            Return True
        ElseIf ((objKunde.NTBHovedKategorier And objNews.NTBHovedKategorier) And Not HEX_SUBKAT) Then
            'Hvis noen Kategorier har treff utenom de med underkategorier:
            Return True
        End If

        If (objKunde.NTBHovedKategorier And (objNews.NTBHovedKategorier And HEX_KUL)) And objKunde.alleKul Then
            Return True
        ElseIf (objKunde.NTBHovedKategorier And (objNews.NTBHovedKategorier And HEX_OKO)) And objKunde.alleOko Then
            Return True
        ElseIf (objKunde.NTBHovedKategorier And (objNews.NTBHovedKategorier And HEX_SPO)) And objKunde.alleSpo Then
            Return True
        End If

        If objKunde.NTBUnderKatKultur > 0 _
            And (objKunde.NTBUnderKatKultur And objNews.NTBUnderKatKultur) Then
            Return True
        End If

        If objKunde.NTBUnderKatKuriosa > 0 _
            And (objKunde.NTBUnderKatKuriosa And objNews.NTBUnderKatKuriosa) Then
            Return True
        End If

        If objKunde.NTBUnderKatOkonomi > 0 _
            And (objKunde.NTBUnderKatOkonomi And objNews.NTBUnderKatOkonomi) Then
            'Or Not (objNews.NTBHovedKategorier And 65536) Then
            Return True
        End If

        If objKunde.NTBUnderKatSport > 0 _
            And (objKunde.NTBUnderKatSport And objNews.NTBUnderKatSport) Then
            'Or Not (objNews.NTBHovedKategorier And 2048) Then
            Return True
        End If
    End Function

    Private Function TestFylker(ByVal objKunde As Customer) As Boolean
        If objKunde.alleOmr Then
            Return True
        ElseIf objKunde.NTBFylker = 0 Then
            'Ingen fylker er valgt:
            Return True
        ElseIf (objKunde.NTBOmraader And (objNews.NTBOmraader And 256)) Then
            If objKunde.alleFyl Then Return True
            'Kun treff p� Norge, sjekk fylke
            If (objKunde.NTBFylker And objNews.NTBFylker) Then
                Return True
            End If
        ElseIf (objKunde.NTBOmraader And objNews.NTBOmraader) Then
            Return True
        End If
        Return False
    End Function

    Private Function DoEpostSms(ByVal strFilename As String, ByVal objKunde As Customer, ByVal isSniffFound As Boolean) As Boolean
        Dim tmpFile As String
        'Dim strErr As String

        'Kopier fil til Kundens mappe

        ' Ikke sende epost p� hastemeldinger 
        If ((objKunde.Prio13Stoffgr And objNews.Prio13Stoffgr) = False) And objKunde.sendEpost And sendEpost Then
            Try
                'E-post utsendelse:
                If isSniffFound Then
                    objNews.SendEpost(objKunde.Epost, objKunde.UthevRegEx(objNews.htmlNews))
                Else
                    objNews.SendEpost(objKunde.Epost)
                End If
                LogFile.WriteLogNoDate(logFilePath, "       E-post sent til: " & objKunde.LogonId & ": " & objKunde.Epost)
            Catch e As Exception
                LogFile.WriteErr(logFilePath, "Feil i E-post utsendelse: " & objKunde.Epost & ": " & Format(objNews.dtTimeStamp, "dd.MM.yyyy HH:mm") & ": " & objNews.ntbInfolinje, e)
                SaveInEpostOffline(objKunde)
                isMailOffline = True
            End Try
        End If

        Dim strOutFile As String
        If ((objKunde.Prio13Stoffgr And objNews.Prio13Stoffgr) Or objKunde.sendSms) And sendSms Then
            Try
                'SMS utsendelse
                strOutFile = strSmsPath & Path.DirectorySeparatorChar _
                            & "SM" & objKunde.Mobilphone & "_" & objNews.ntbId.Replace(":", "-") & ".txt"

                Dim smsOut As String = "[SMS]" & vbCrLf _
                                    & "To=" & objKunde.Mobilphone & vbCrLf _
                                    & "From=" & objNews.smsFrom & vbCrLf _
                                    & "Text=" & objNews.smsNews
                LogFile.WriteFile(strOutFile, smsOut)
                LogFile.WriteLogNoDate(logFilePath, "       SMS sent til   : " & objKunde.LogonId & ": " & objKunde.Mobilphone)

                ' Oppdatering av Wap index fil
                objKunde.objWapIndex.UpdateWapIndex(objNews.indexFileRef, objNews.ntbInfolinje, objNews.dtTimeStamp, 15)
            Catch e As Exception
                LogFile.WriteErr(logFilePath, "Feil i SMS utsendelse: " & strOutFile, e)
            End Try
        End If
        Return True
    End Function

    Private Sub SaveInEpostOffline(ByVal objKunde As Customer)
        Dim strOutFile As String = strMailOfflinePath & Path.DirectorySeparatorChar _
            & objKunde.LogonId & "_" & objNews.ntbId & ".txt"

        Dim mailOut As String = objKunde.Epost & vbCrLf _
                                & objNews.message.From & vbCrLf _
                                & objNews.message.Subject & vbCrLf _
                                & objNews.message.Body

        LogFile.WriteFile(strOutFile, mailOut)
        LogFile.WriteLogNoDate(logFilePath, "    *** E-post lagt i offline-file: " & strOutFile)
    End Sub

    Private Sub SendEpostFromOffline()
        Dim mail As System.Web.Mail.SmtpMail
        mail.SmtpServer = smtpHost

        Dim arrFileList() As String
        'Read all files in folder
        Try
            arrFileList = Directory.GetFiles(strMailOfflinePath)
            For Each strInputFile As String In arrFileList
                Dim message As New System.Web.Mail.MailMessage
                message.BodyFormat = Web.Mail.MailFormat.Html

                Dim sr As New StreamReader(strInputFile, myEncoding)
                message.To = sr.ReadLine
                message.From = sr.ReadLine
                message.Subject = sr.ReadLine
                message.Body = sr.ReadToEnd
                sr.Close()

                mail.Send(message)
                LogFile.WriteLogNoDate(logFilePath, "    E-post sendt fra offline-file: " & strInputFile)
                File.Delete(strInputFile)
            Next
            isMailOffline = False
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "E-postserver fremdeles offline", ex)
        End Try

    End Sub

    Public Sub FillOneDataSet(ByRef dsDataSet As DataSet, ByVal strOfflineFile As String, ByVal strCommand As String, ByVal dataAdapter As SqlDataAdapter, ByVal command As SqlCommand, Optional ByVal strGroupListID As String = "")
        Dim strFile As String = databaseOfflinePath & "\" & strOfflineFile & strGroupListID & ".xml"
        command.CommandText = strCommand
        Try
            dataAdapter.Fill(dsDataSet)
            dsDataSet.WriteXml(strFile)
        Catch ex As Exception
            dsDataSet.ReadXml(strFile)
            LogFile.WriteErr(logFilePath, "Feilet i FillOneDataSet: ", ex)
        End Try
    End Sub

    Public Sub ReInitKunde()
        FillDataSets()
        FillCustomerHt()
        LogFile.WriteLog(logFilePath, "**** Database lest og kunde-objektene er reinitalisert.")
    End Sub

    Private Sub TimerUpdateFromDb_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles TimerUpdateFromDb.Elapsed
        Try
            mut.WaitOne()
            Me.StopEvents()
            mut.ReleaseMutex()
            Static deleted As Boolean

            If TestWapCustomerUpdate() Then
                Me.ReInitKunde()
            End If

            If isMailOffline Then
                Me.SendEpostFromOffline()
            End If

            If Now.Hour = 1 Then
                ' Kj�rer en gang i d�gnet etter klokka 01:00
                If Not deleted Then
                    DeleteOldWap(beholdWapDager)
                    deleted = True
                End If
            Else
                deleted = False
            End If

            Me.DoAllInputFiles()
            Me.StartEvents()

        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feilet i SmsWpost.TimerUpdateFromDb_Elapsed: ", ex)
        End Try
    End Sub

    Public Sub StartEvents()
        FileSystemWatcher1.EnableRaisingEvents = True
        TimerUpdateFromDb.Start()
        Me.isStarted = True
    End Sub

    Public Sub StopEvents()
        FileSystemWatcher1.EnableRaisingEvents = False
        TimerUpdateFromDb.Stop()
        Me.isStarted = False
    End Sub

    Public Function WaitWhileBusy(ByVal SleepRetrySeconds As Integer, Optional ByVal TimeOutSeconds As Integer = 15) As Boolean
        WaitWhileBusy = mut.WaitOne(TimeOutSeconds * 1000, True)
        mut.ReleaseMutex()
    End Function

    Public Sub DeleteOldWap(Optional ByVal beholdDager As Integer = 7)
        Try
            LogFile.WriteLog(logFilePath, "----------------------------------------------------------------")
            LogFile.WriteLog(logFilePath, "DeleteOldWap startet. ")

            defaultWapIndex.DeleteOldLines(beholdDager)

            For Each objKunde As Customer In htKunde.Values
                If objKunde.sendSms Then
                    objKunde.objWapIndex.DeleteOldLines(beholdDager)
                End If
            Next

            Dim arrFileList() As String = Directory.GetFiles(wapPath & "\" & wmlNewsPath, "*.wml")
            For Each strInputFile As String In arrFileList
                Dim fileDate As Date = File.GetLastWriteTime(strInputFile)
                If Date.Compare(fileDate.AddDays(beholdDager + 1), Now) < 0 Then
                    File.Delete(strInputFile)
                    LogFile.WriteLog(logFilePath, "Sletter gammel WAP-fil: " & strInputFile)
                End If
            Next
            LogFile.WriteLog(logFilePath, "DeleteOldWap ferdig. ")
            LogFile.WriteLog(logFilePath, "----------------------------------------------------------------")

        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feilet i SmsEpost.DeleteOldWap: ", ex)
        End Try
    End Sub

End Class
