Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings

Public Class Customer

    Public objWapIndex As WapIndex
    Private Const END_CODE As String = "<font class='viewUthev'>"
    Private Const START_CODE As String = "</font>"
    Private strRegExReplace As String = END_CODE & "$&" & START_CODE
    Private regExStartPattern As String = "(\b"
    ' slutt pattern med norsk stemming av substantiver:
    Private regExEndPattern As String = "(er|ene|e|en|et|a|s|ne|t|r)?\b)"
    Private logFilePath As String = AppSettings("logFilePath")

    Private regExSniff As Regex
    Private regExUthev As Regex

    Private rowCust As DataRow
    Private objBitnames As GetBitNames2
    Public regExPattern As String
    Public Id As Integer
    Public LogonId As String
    Public Mobilphone As String
    Public Epost As String
    Public sendSms As Boolean
    Public sendEpost As Boolean
    Public doSniff As Boolean
    Private NTBSniffertxt As String

    Public Prio13Stoffgr As Integer
    Public NTBStoffgrupper As Integer
    Public NTBOmraader As Integer
    Public NTBFylker As Integer
    Public NTBHovedKategorier As Integer

    Public alleOmr As Boolean
    Public alleFyl As Boolean
    Public alleHov As Boolean
    Public alleSpo As Boolean
    Public alleOko As Boolean
    Public alleKul As Boolean

    Public NTBUnderKatSport As Long
    Public NTBUnderKatOkonomi As Long
    Public NTBUnderKatKuriosa As Long
    Public NTBUnderKatKultur As Long

    Public Sub New(ByRef row As DataRow, ByRef bitnames As GetBitNames2)
        objBitnames = bitnames
        init(row)
    End Sub

    Public Sub init(ByVal row As DataRow)
        rowCust = row ' Set global row

        Id = rowCust("ID")
        LogonId = rowCust("LogonId") & ""
        Mobilphone = rowCust("Mobilphone") & ""
        Mobilphone = Mobilphone.Trim()
        If Mobilphone.StartsWith("+") Then
            Mobilphone = "00" & Mobilphone.Substring(1)
        ElseIf Not Mobilphone.StartsWith("00") Then
            Mobilphone = "0047" & Mobilphone
        End If

        Dim tempMobile As String
        If Mobilphone.StartsWith("0047") Then
            tempMobile = Mobilphone.Substring(4)
        Else
            tempMobile = Mobilphone
        End If

        Epost = rowCust("Epost") & ""

        sendEpost = rowCust("SendEpost")
        sendSms = rowCust("SMS")

        NTBSniffertxt = rowCust("NTBSniffertxt") & ""
        NTBSniffertxt = NTBSniffertxt.Trim
        If NTBSniffertxt <> "" Then
            MakeRegEx()
        End If

        alleOmr = Convert.ToString(rowCust("NTBOmraader")).StartsWith("AlleOmr")
        alleFyl = Convert.ToString(rowCust("NTBFylker")).StartsWith("AlleFyl")
        alleHov = Convert.ToString(rowCust("NTBHovedKategorier")).StartsWith("AlleHov")
        alleSpo = Convert.ToString(rowCust("NTBUnderKatSport")).StartsWith("AlleSpo")
        alleOko = Convert.ToString(rowCust("NTBUnderKatOkonomi")).StartsWith("AlleOko")
        alleKul = Convert.ToString(rowCust("NTBUnderKatKultur")).StartsWith("AlleKul")

        Prio13Stoffgr = objBitnames.GetBitMapSmsKunde(rowCust("Prio13Stoffgr") & "")
        'sendSms = sendSms Or Prio13Stoffgr <> 0

        NTBStoffgrupper = objBitnames.GetBitMapSmsKunde(rowCust("NTBStoffgrupper") & "")
        NTBOmraader = objBitnames.GetBitMapSmsKunde(rowCust("NTBOmraader") & "")
        NTBFylker = objBitnames.GetBitSubCatSmsKunde(rowCust("NTBFylker") & "")
        NTBHovedKategorier = objBitnames.GetBitMapSmsKunde(row("NTBHovedKategorier") & "")

        NTBUnderKatSport = objBitnames.GetBitSubCatSmsKunde(rowCust("NTBUnderKatSport") & "")
        NTBUnderKatOkonomi = objBitnames.GetBitSubCatSmsKunde(rowCust("NTBUnderKatOkonomi") & "")
        NTBUnderKatKuriosa = objBitnames.GetBitSubCatSmsKunde(rowCust("NTBUnderKatKuriosa") & "")
        NTBUnderKatKultur = objBitnames.GetBitSubCatSmsKunde(rowCust("NTBUnderKatKultur") & "")

        If sendSms Or Prio13Stoffgr <> 0 Then
            objWapIndex = New WapIndex(tempMobile)
        End If

    End Sub

    Private Sub MakeRegEx()
        Try
            Dim sniff As String = NTBSniffertxt.Replace(";", "|")
            sniff = sniff.Replace("| ", "|")
            sniff = sniff.Replace(" |", "|")
            sniff = sniff.Replace(".", "")
            sniff = sniff.Replace("?", "")
            sniff = sniff.Replace("*", ".*?") ' St�tte for trunkering

            If sniff.EndsWith("|") Then
                sniff = sniff.Substring(0, sniff.Length - 1)
            End If
            If sniff.StartsWith("|") Then
                sniff = sniff.Substring(1)
            End If
            sniff = "(" & sniff & ")"
            regExPattern = regExStartPattern & sniff & regExEndPattern
            regExSniff = New Regex(regExPattern, RegexOptions.IgnoreCase)

            'Lag RegEx for uthevning av s�keord
            Dim exceptionList As String = "\b(font|style|td|tr|table|border|content|html|" _
                    & "head|body|div|class|normal|color|family|text|background|" _
                    & "decoration|line|margin|bottom|top|weight|size|height|meta)\b"

            'Fjerner HTML-kode:
            sniff = Regex.Replace(sniff, exceptionList, "", RegexOptions.IgnoreCase)
            'Fjerner paranteser og doble ||):
            sniff = Regex.Replace(sniff, "(\|\||\(\||\|\)|\)|\()", "")

            regExPattern = regExStartPattern & "(" & sniff & ")" & regExEndPattern
            regExUthev = New Regex(regExPattern, RegexOptions.IgnoreCase)
            doSniff = True
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i Customer.MakeRegEx: " & LogonId & ": Sniffertxt=" & NTBSniffertxt, ex)
        End Try
    End Sub

    Public Function FinnSniff(ByVal testTekst As String) As Boolean
        Try
            Return regExSniff.IsMatch(testTekst)
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i Customer.FinnSniff: " & LogonId & ": Sniffertxt=" & NTBSniffertxt, ex)
        End Try
    End Function

    Public Function UthevRegEx(ByVal doc As String) As String
        ' For uthevning av s�keord i HTML fil 
        ' Flere s�keord kan bringes inn kommaseparert.
        Try
            Return regExUthev.Replace(doc, strRegExReplace)
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i Customer.UthevRegEx: " & LogonId & ": Sniffertxt=" & NTBSniffertxt, ex)
        End Try
    End Function
End Class
