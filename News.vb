Imports System.Xml
Imports System.Configuration.ConfigurationSettings
Imports System.Text
Imports System.IO
Imports System.Web.Mail

Public Class News
    Private xmlDoc As New XmlDocument

    Public ntbId As String
    Public ntbInfolinje As String
    Public ntbStikkord As String
    Public txtNews As String
    Public dtTimeStamp As Date
    Public htmlNews As String
    Private xhtmlWapNews As String
    Private xhtmlWapNewsFileName As String
    Public smsNews As String
    Public smsFrom As String
    Public indexFileRef As String

    Public Prio13Stoffgr As Integer
    Public NTBStoffgrupper As Integer
    Public NTBUndergrupper As Integer
    Public NTBOmraader As Integer
    Public NTBFylker As Integer
    Public NTBHovedKategorier As Integer
    Public NTBTjeneste As Integer

    Public NTBUnderKatSport As Long
    Public NTBUnderKatOkonomi As Long
    Public NTBUnderKatKuriosa As Long
    Public NTBUnderKatKultur As Long

    Public message As MailMessage

    Private logFilePath As String = AppSettings("logFilePath")
    Private smtpHost As String = AppSettings("smtpHost")
    Private messageFrom As String = AppSettings("messageFrom")
    Private dontIgnoreMeldingstype As String = AppSettings("dontIgnoreMeldingstype")

    Private myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")

    Private wapPath As String = AppSettings("wapPath")
    Private wmlNewsPath As String = AppSettings("wmlNewsPath")
    Private wapWmlPath As String = wapPath & "\" & wmlNewsPath
    Private fileExt As String = ".wml"

    Public Sub New(ByVal fileName As String, ByRef bitnames As GetBitNames2, ByVal objXsltProc As XsltProc, ByRef testNtbTjeneste As Integer, ByRef testNtbMeldingstype As Integer)
        ' -----------------------------------------------------------------------------
        ' New(ByVal fileName As String, 
        '   ByRef bitnames As GetBitNames2, 
        '   ByVal objXsltProc As XsltProc, 
        '   ByRef testNtbTjeneste As Integer, 
        '   ByRef testNtbMeldingstype As Integer)
        '
        ' Description: Starts the new News object
        '
        ' Parameters:   filename
        '				objXsltProc
        '               testNtbTjeneste
        '               testNtbMeldingstype
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '           30.05.2008, Trond Hus�, NTB: Added Try statement around the ntbStikkord-fetch.
        '
        ' -----------------------------------------------------------------------------

        ' Get the XML-document to get information from
        xmlDoc.Load(fileName)

        ' Using XPATH to get the the TimeStamp from the XML-document
        Try
            dtTimeStamp = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='timestamp']/@content").InnerText
        Catch ex As Exception
            dtTimeStamp = Now
        End Try

        ' Getting the Title from the XML-document.
        Try
            ntbInfolinje = xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText
        Catch
            ' If there is no title, it could be because this is a message meant for NTB-Tikkertjeneste
            ntbInfolinje = ""
        End Try

        ' Getting the Keyword from the XML-document
        Try
            ntbStikkord = xmlDoc.SelectSingleNode("/nitf/head/docdata/key-list/keyword/@key").InnerText
        Catch
            ntbStikkord = ""
        End Try


        'Sjekk tjenestetype
        NTBTjeneste = bitnames.GetBitFolders(xmlDoc.SelectNodes("/nitf/head/meta[@name='NTBTjeneste']/@content"))
        If (testNtbTjeneste And NTBTjeneste) = 0 Then
            testNtbTjeneste = NTBTjeneste * -1
            Exit Sub
        End If

        MakeBitmaps(bitnames)

        'Sjekk meldingstype
        If (testNtbMeldingstype And NTBUndergrupper) Then
            If (xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBMeldingsType']/@content").InnerText <> dontIgnoreMeldingstype) Then
                testNtbMeldingstype = testNtbMeldingstype * -1
            End If
        End If

        htmlNews = objXsltProc.DoXslTransform(xmlDoc, XsltProc.xsltType.html)
        'XHTL-format for IndexServer WAP-hent l�sning:
        xhtmlWapNews = objXsltProc.DoXslTransform(xmlDoc, XsltProc.xsltType.wap)
        xhtmlWapNewsFileName = MakeXhtmlWapNewsFileName(xmlDoc)

        ' Calling routines to create different type of news.
        MakeWapNews() ' Listing 10 articles in the index wap-file
        MakeNewMessage() ' Create New message
        MakeTextNews() ' Create Text message
        MakeSmsNews() ' Create SMS-news.
    End Sub

    Private Function MakeXhtmlWapNewsFileName(ByRef xmlDoc As Xml.XmlDocument)
        ' -----------------------------------------------------------------------------
        ' MakeXhtmlWapNewsFileName
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   objBitnames
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        'Lager kort filnavn for WAP xhtml for for indexserver-l�sningen.
        Dim fileName As String
        Try
            'fileName = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='timestamp']/@content").Value.Substring(0, 10).Replace(".", "")
            'Kun dag (i fra dato)
            fileName = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='timestamp']/@content").Value.Substring(8, 2).Replace(".", "")
            'Kun f�rste bokstav i stoffgruppe
            fileName &= xmlDoc.SelectSingleNode("//tobject/@tobject.type").Value.Substring(0, 1)
            Dim prioritet As String = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBPrioritet']/@content").Value
            If prioritet < "4" Then
                'Kun for at haste-meldinger ikke skal bli overskrevet:
                fileName &= prioritet
            End If
            fileName &= "_" & xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBStikkord']/@content").Value
            fileName = fileName.ToLower

            fileName = fileName.Replace("�", "a")
            fileName = fileName.Replace("�", "o")
            fileName = fileName.Replace("�", "a")
            fileName = fileName.Replace("�", "o")
            fileName = fileName.Replace("�", "a")
            fileName = fileName.Replace("�", "e")
            fileName = fileName.Replace("�", "e")
            fileName = fileName.Replace("�", "u")

            fileName = fileName.Replace("'", "_")
            fileName = fileName.Replace("""", "_")
            fileName = fileName.Replace("\", "_")
            fileName = fileName.Replace("/", "_")
            fileName = fileName.Replace(":", "_")
            fileName = fileName.Replace(".", "_")

            For Each ch As Char In Path.InvalidPathChars
                fileName = fileName.Replace(ch, "_")
            Next
        Catch ex As Exception
            fileName = Format(Now, "yyyyMMdd_HHmmss_ffff")
        End Try


        fileName &= ".htm"

        Return fileName

    End Function

    Private Sub MakeBitmaps(ByVal objBitnames As GetBitNames2)
        ' -----------------------------------------------------------------------------
        ' MakeBitmaps
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   objBitnames
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        Try
            'On Error Resume Next

            ntbId = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBID']/@content").InnerText

            Dim edUrgency As Integer = Val(xmlDoc.SelectSingleNode("/nitf/head/docdata/urgency/@ed-urg").InnerText)
            NTBStoffgrupper = objBitnames.GetBitCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/@tobject.type"))
            If edUrgency < "4" Then
                Prio13Stoffgr = NTBStoffgrupper
                smsFrom = "NTB HAST"
            Else
                Prio13Stoffgr = 0
                smsFrom = "NTB"
            End If

            NTBUndergrupper = objBitnames.GetBitCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/tobject.property/@tobject.property.type"))

            NTBOmraader = objBitnames.GetBitCatNitf(xmlDoc.SelectNodes("/nitf/head/docdata/evloc/@state-prov"))
            NTBFylker = objBitnames.GetBitSubCatNitf(xmlDoc.SelectNodes("/nitf/head/docdata/evloc/@county-dist"))

            NTBHovedKategorier = objBitnames.GetBitCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject/@tobject.subject.code"))

            NTBUnderKatSport = objBitnames.GetBitSubCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter"))
            NTBUnderKatOkonomi = objBitnames.GetBitSubCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject[@tobject.subject.code='OKO']/@tobject.subject.matter"))
            NTBUnderKatKuriosa = objBitnames.GetBitSubCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter"))
            NTBUnderKatKultur = objBitnames.GetBitSubCatNitf(xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter"))

        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i News.MakeBitmaps: ", ex)
        End Try
    End Sub

    Private Sub MakeNewMessage()
        ' -----------------------------------------------------------------------------
        ' MakeNewMessage
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   None.
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        message = New MailMessage
        message.From = messageFrom
        message.BodyEncoding = myEncoding

        Dim subjectHead As String

        Try
            message.Subject = smsFrom & ": " _
            & xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").InnerText & ": " _
            & xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText
        Catch
            message.Subject = "Nyhet fra " & smsFrom
        End Try

        message.BodyFormat = MailFormat.Html
        message.Body = htmlNews

    End Sub

    Private Sub MakeTextNews()
        ' -----------------------------------------------------------------------------
        ' MakeTextNews
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   None.
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        Dim sbTekst As New System.Text.StringBuilder
        Try
            Dim nodeList As XmlNodeList = xmlDoc.SelectNodes("/nitf/body/body.head/hedline/hl1 | /nitf/body/body.content/*")
            For Each node As XmlNode In nodeList
                sbTekst.Append(node.InnerText & vbCrLf)
            Next
            txtNews = sbTekst.ToString
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i MakeTextNews", ex)
        End Try
    End Sub

    Private Sub MakeSmsNews()
        ' -----------------------------------------------------------------------------
        ' MakeSmsNews
        '
        ' Description: Creates SMS news that will be sent to customers mobilephone.
        '
        ' Parameters:   None.
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------

        ' Getting the information from the XML-document.
        ' This checks to see if we have text in field SMSTextFelt (NON NITF 3.2-standard).
        '               The above is true when the journalist creates a HAST-message. 
        ' or in ingress p[@lede='true']


        Try
            Dim nodeList As XmlNodeList = xmlDoc.SelectNodes("/nitf/head/meta[@name='SMSTextFelt']/@content " _
                & "| /nitf/body/body.content/p[@lede='true'] | /nitf/body/body.content/p[1]")

            ' Getting the headline / title
            If nodeList.Count = 0 Then
                nodeList = xmlDoc.SelectNodes("/nitf/body/body.head/hedline/hl1")
            ElseIf nodeList.Item(0).InnerText.Trim.Length = 0 Then
                nodeList = xmlDoc.SelectNodes("/nitf/body/body.head/hedline/hl1")
            End If

            ' Grab the text from the first node that we've found. 
            smsNews = nodeList.Item(0).InnerText

            ' If the length is larger than 145 characters we cut the string and add ... to it. 
            ' SMS-messages shall not be more than 160 characters. 
            If smsNews.Length > "145" Then
                smsNews = smsNews.Substring(0, 145) & "..."
            End If
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Feil i MakeSmsNews", ex)
        End Try
    End Sub

    Public Function SendEpost(ByVal messageTo As String) As Boolean
         -----------------------------------------------------------------------------
        ' SendEpost
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   
        '               messageTo
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        Dim mail As SmtpMail
        mail.SmtpServer = smtpHost
        message.To = messageTo
        mail.Send(message)
    End Function

    Public Function SendEpost(ByVal messageTo As String, ByVal body As String) As Boolean
        ' -----------------------------------------------------------------------------
        ' SendEpost
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   
        '               messageTo
        '               body
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        Dim mail As SmtpMail
        mail.SmtpServer = smtpHost
        Dim msg As New MailMessage
        msg = message
        msg.To = messageTo
        msg.Body = body
        mail.Send(msg)
    End Function

    Private Sub MakeWapNews()
        ' -----------------------------------------------------------------------------
        ' MakeWapNews
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   
        '               none
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------

        Dim mobileBufferSize As Integer = AppSettings("mobileBufferSize")
        indexFileRef = "../" & wmlNewsPath & "/" & ntbId & fileExt

        Const WML_HEADER As String = "<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf _
                                    & "<!DOCTYPE wml PUBLIC '-//WAPFORUM//DTD WML 1.1//EN' 'http://www.wapforum.org/DTD/wml_1.1.xml'>" _
                                    & "<wml>" & vbCrLf _
                                    & "<template>" & vbCrLf _
                                    & "<do type='prev' name='Previous' label='&lt;- Forrige side'><prev/></do>" & vbCrLf

        Const WML_FOOTER As String = "</card>" & vbCrLf _
                                    & "</wml>" & vbCrLf

        Dim WML_CARD As String = "</template>" & vbCrLf _
                            & "<card id='Card01' title='" & ntbStikkord & "'>" & vbCrLf
        Dim headFootLen As Integer = WML_HEADER.Length + WML_CARD.Length + WML_FOOTER.Length

        Dim pNodeList As XmlNodeList = xmlDoc.SelectNodes("/nitf/body/body.content/p |" _
                                            & "/nitf/body/body.content/hl2 |" _
                                            & "/nitf/body/body.content/table/tr |" _
                                            & " /nitf/body/body.end/tagline")
        Dim prev As Integer = 0
        Dim isBufferSize As Boolean = False
        Dim docFragNum As Integer = 1
        Dim doNextDocFrag As Boolean
        Do
            ' Deler dokumentet i flere WML-sider med brudd i kun hele avsnitt:
            Dim sbDoc As New StringBuilder("")
            If docFragNum = 1 Then
                sbDoc.Append("<p><strong>" & ntbInfolinje.Replace("&", "&amp;") & "</strong></p><p>&#160;</p>")
            End If
            Dim strDoNext As String = "<do type='accept' name='Next' label='Neste side -&gt;'>" _
                          & "<go href='" & ntbId & "_" & docFragNum & fileExt & "'/></do>" & vbCrLf
            Dim strANext As String = "<p>.... <a href='" & ntbId & "_" & docFragNum & fileExt & "'>Neste side -&gt;</a></p>" & vbCrLf
            isBufferSize = False
            Dim headFootDoNextLen As Integer = headFootLen + strDoNext.Length + strANext.Length + "<p>&#160;&#160;&#160;</p>  ".Length

            Dim i As Integer
            For i = prev To pNodeList.Count - 1
                Dim docLength As Integer = headFootDoNextLen + sbDoc.Length
                If pNodeList.Item(i).InnerText.Length + docLength > mobileBufferSize Then
                    If i = prev Then
                        ' WML-dok med kun 1. avsintt er st�rre enn buffer
                        sbDoc.Append("<p>" & pNodeList.Item(i).InnerText.Substring(0, mobileBufferSize - docLength).Replace("&", "&amp;") & "</p>" & vbCrLf)

                        prev = i + 1
                    Else
                        prev = i
                    End If
                    isBufferSize = True
                    Exit For
                End If
                If pNodeList.Item(i).InnerText.Trim <> "" Then
                    If i = 0 Then
                        ' Spesielt for f�rste avsnitt=ingressen:
                        sbDoc.Append("<p><strong>" & pNodeList.Item(i).InnerText.Replace("&", "&amp;") & "</strong></p>" & vbCrLf)
                    ElseIf pNodeList.Item(i).Name = "hl2" Then
                        sbDoc.Append("<p><br/><strong>" & pNodeList.Item(i).InnerText.Replace("&", "&amp;") & "</strong></p>" & vbCrLf)
                    ElseIf pNodeList.Item(i).Name = "tagline" Then
                        ' Epostadressen som href:
                        sbDoc.Append("<p><br/>" & pNodeList.Item(i).InnerText & "</p>" & vbCrLf)
                    Else
                        sbDoc.Append("<p>&#160;&#160;&#160;" & pNodeList.Item(i).InnerText.Replace("&", "&amp;") & "</p>" & vbCrLf)
                    End If
                End If
            Next

            If docFragNum > 8 Then
                sbDoc.Insert(0, WML_HEADER & WML_CARD)
                sbDoc.Append("<p>...... (forkortet til maks. 10 WAP-sider)</p>" & vbCrLf)
                sbDoc.Append(WML_FOOTER)
                doNextDocFrag = False
            ElseIf isBufferSize Then
                sbDoc.Insert(0, WML_HEADER & strDoNext & WML_CARD)
                sbDoc.Append(strANext)
                sbDoc.Append(WML_FOOTER)
                doNextDocFrag = True
            Else
                sbDoc.Insert(0, WML_HEADER & WML_CARD)
                sbDoc.Append(WML_FOOTER)
                doNextDocFrag = False
            End If

            WriteWmlFiles(sbDoc, Me.dtTimeStamp, docFragNum)
            docFragNum += 1

        Loop While doNextDocFrag
    End Sub

    Public Sub WriteWmlFiles(ByVal sbDoc As StringBuilder, ByVal timeStamp As Date, ByVal docFragNum As Integer)
        ' -----------------------------------------------------------------------------
        ' WriteWmlFiles
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   
        '               sbDoc
        '               timeStamp
        '               docFragNum
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        Dim docFrag As String
        If docFragNum = 1 Then
            docFrag = ""
        Else
            docFrag = "_" & docFragNum - 1
        End If

        Dim fileName As String = wapWmlPath & "\" & ntbId & docFrag & fileExt

        LogFile.WriteFile(fileName, sbDoc.ToString)
        File.SetLastWriteTime(fileName, timeStamp)
        LogFile.WriteLogNoDate(logFilePath, "       Wap filer skrevet: " & fileName)
    End Sub

    Public Function WriteXhtmlWap(ByVal path As String) As Boolean
        ' -----------------------------------------------------------------------------
        ' WriteXhtmlWap
        '
        ' Description: Creates Text News message.
        '
        ' Parameters:   path
        '
        ' Returns:
        '
        ' Notes :
        '           30.05.2008, Trond Hus�, NTB: Added default documentation template.
        '
        ' -----------------------------------------------------------------------------
        Dim file As String = path & "\" & xhtmlWapNewsFileName
        LogFile.WriteFile(file, xhtmlWapNews)

    End Function

End Class
